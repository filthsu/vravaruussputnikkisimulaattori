﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Main : MonoBehaviour
{
    public static Main Instance { get; private set; }
    
    public string m_gameScene;
    public string m_mainMenuScene;
    public Game m_gamePrefab;
    public Player m_playerPrefab;

    Player m_menuPlayer;

    private void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        InitializeMenuScene();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartGameSession();
        }

        if(null != m_menuPlayer)
        {
            Hand[] hands = m_menuPlayer.GetComponentsInChildren<Hand>();
            for(int i = 0; i < hands.Length; i++)
            {
                if(hands[i].m_side == Hand.Side.Right)
                {
                    if(hands[i].m_skeleton.fingerCurls[0] < .1f)
                    {
                        bool thumbUp = true;
                        for(int f = 1; f < 4; f++)
                        {
                            if(hands[i].m_skeleton.fingerCurls[f] < .98f)
                            {
                                thumbUp = false;
                                break;
                            }
                        }
                        if (thumbUp)
                        {
                            StartGameSession();
                        }
                    }
                }
            }
        }
    }

    private void InitializeMenuScene()
    {
        SceneManager.LoadScene(m_mainMenuScene, LoadSceneMode.Additive);
        m_menuPlayer = Instantiate(m_playerPrefab);
    }

    public void StartGameSession()
    {
        if(null == m_gameSession && !m_isGameRunning)
        {
            m_gameSession = StartCoroutine(GameSession());
        }
    }

    public void EndGameSession()
    {
        m_isGameRunning = false;
    }

    #region Run game
    private bool m_isGameRunning = false;
    private Coroutine m_gameSession;

    private IEnumerator GameSession()
    {
        m_isGameRunning = true;
        if (m_menuPlayer)
        {
            Destroy(m_menuPlayer.gameObject);
        }
        yield return SceneManager.UnloadSceneAsync(m_mainMenuScene);
        yield return SceneManager.LoadSceneAsync(m_gameScene, LoadSceneMode.Additive);
        SceneManager.SetActiveScene(SceneManager.GetSceneAt(1));
        Instantiate(m_gamePrefab);
        while(m_isGameRunning)
        {


            yield return null;
        }

        yield return SceneManager.UnloadSceneAsync(m_gameScene);

        InitializeMenuScene();

        m_gameSession = null;
    }
    #endregion
}
