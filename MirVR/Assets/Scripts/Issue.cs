﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Issue : MonoBehaviour
{
    public bool Active { get; set; }
    public Game m_game;
    public int m_resolveScore = 100;
    public float m_severity = .1f;

    public virtual void Resolve()
    {
        Active = false;
        m_game.OnIssueResolved(this);
    }

}
