﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio : MonoBehaviour
{
    static Audio Instance
    {
        get; set;
    }

    Queue<AudioSource> m_pool;

    private void Awake()
    {
        Instance = this;
        m_pool = new Queue<AudioSource>();
    }

    private void ExtendPool(int size = 1)
    {
        for(int i = 0; i < size; i++)
        {
            GameObject go = new GameObject("AudioClip");
            AudioSource src = go.AddComponent<AudioSource>();
            src.playOnAwake = false;
            src.loop = false;
            m_pool.Enqueue(src);
        }
    }

    private IEnumerator HandleSourcePooling(AudioSource src)
    {
        if (null == src.clip) yield return null;
        else yield return new WaitForSeconds(src.clip.length);
        m_pool.Enqueue(src);
        yield break;
    }

    public static void Play3D(AudioClip clip, Vector3 position, float volume = 1.0f)
    {
        if(Instance.m_pool.Count <=0) { Instance.ExtendPool(); }

        AudioSource src = Instance.m_pool.Dequeue();
        src.transform.position = position;
        src.volume = volume;
        src.spatialBlend = 1.0f;

        src.clip = clip;
        src.Play();
        Instance.StartCoroutine(Instance.HandleSourcePooling(src));
    }

    public static void Play2D(AudioClip clip, float volume = 1.0f)
    {
        if (Instance.m_pool.Count <= 0) { Instance.ExtendPool(); }

        AudioSource src = Instance.m_pool.Dequeue();
        src.volume = volume;
        src.spatialBlend = 0.0f;

        src.clip = clip;
        src.Play();
        Instance.StartCoroutine(Instance.HandleSourcePooling(src));
    }
}
