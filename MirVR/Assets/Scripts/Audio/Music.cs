﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour
{
    private static Music Instance { get; set; }
    public AudioClip[] m_songs;
    private int m_songIndex;
    private float m_playTime;
    public bool IsPlaying { get; private set; }
    AudioSource m_source;

    private void Awake()
    {
        Instance = this;
        m_source = gameObject.AddComponent<AudioSource>();
        m_source.loop = false;
        m_source.playOnAwake = false;
        m_source.spatialBlend = 0.0f;
    }

    // Start is called before the first frame update
    void Start()
    {
        if(m_songs.Length > 0)
        {
            int rnd = Random.Range(0, m_songs.Length);
            PlaySong(rnd);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (IsPlaying)
        {
            m_playTime += Time.deltaTime;
            if(m_playTime > m_source.clip.length)
            {
                int rnd = Random.Range(0, m_songs.Length);
                PlaySong(rnd);
            }
        }
    }

    /// <summary>
    /// Immediately plays a specified song
    /// </summary>
    public static void PlaySong(int index)
    {
        Instance.m_source.clip = Instance.m_songs[index];
        Instance.m_songIndex = index;
        Instance.m_playTime = 0;
        Instance.m_source.Play();
        Instance.IsPlaying = true;
    }

    public static void Stop()
    {
        Instance.IsPlaying = false;
        Instance.m_source.Stop();
        Instance.m_playTime = 0;
    }
}
