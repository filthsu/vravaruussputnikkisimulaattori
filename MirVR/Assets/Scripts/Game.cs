﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    public static Game Current { get; private set; }

    [System.Serializable]
    public class GameBalance
    {
        public int m_pointsPerSecond = 10;
        public float m_issueFrequency = 5;
        public float m_issueFrequencyChange = .2f;

    }
    public GameBalance m_balance;

    [System.Serializable]
    public class GameScore
    {
        public float m_score;
        public int m_issuesResolved;
    }


    private Player m_player;

    public GameScore m_score;

    private Issue[] m_availableIssues;
    private List<Issue> m_activeIssues;

    public float m_shipHealth = 1.0f;
  
    private float m_timeUntilNextIssue = 5;

    // Start is called before the first frame update
    void Start()
    {
        Current = this;
        m_player = Instantiate(Main.Instance.m_playerPrefab, transform);
        m_player.transform.position = new Vector3(0, 0, 0);
        m_availableIssues = FindObjectsOfType<Issue>();
        m_activeIssues = new List<Issue>();
        for(int i = 0; i < m_availableIssues.Length; i++)
        {
            m_availableIssues[i].m_game = this;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (m_shipHealth > 0)
        {
            m_timeUntilNextIssue -= Time.deltaTime;
            if (m_timeUntilNextIssue <= 0 && Random.value < .2f)
            {
                ActivateRandomIssue();
                m_timeUntilNextIssue = m_balance.m_issueFrequency;
                m_balance.m_issueFrequency = Mathf.MoveTowards(m_balance.m_issueFrequency, 1, m_balance.m_issueFrequencyChange);
            }

            for (int i = 0; i < m_activeIssues.Count; i++)
            {
                m_shipHealth -= Time.deltaTime * m_activeIssues[i].m_severity;
                if(m_shipHealth <= 0)
                {
                    Debug.Log("Ship död.");
                    break;
                }
            }

            m_score.m_score += Time.deltaTime * m_balance.m_pointsPerSecond;
        }
    }

    public void OnIssueActivated(Issue issue)
    {
        m_activeIssues.Add(issue);
    }

    public void OnIssueResolved(Issue issue)
    {
        m_activeIssues.Remove(issue);
        m_score.m_issuesResolved++;
        m_score.m_score += issue.m_resolveScore;
    }

    private void ActivateRandomIssue()
    {
        int index = Random.Range(0, m_availableIssues.Length);
        if (m_availableIssues[index].Active) return;

        m_availableIssues[index].Active = true;
        OnIssueActivated(m_availableIssues[index]);
    }
    
    public void EndGame()
    {
        Main.Instance.EndGameSession();
    }
}
