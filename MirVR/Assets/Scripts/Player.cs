﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Transform m_cameraTransform;
    public Rigidbody m_rigidbody;

    Quaternion m_oldCameraRotation;

    public CapsuleCollider m_collider;
    // Start is called before the first frame update
    void Start()
    {
        if(!m_rigidbody)
            m_rigidbody = GetComponent<Rigidbody>();   
    }

    // Update is called once per frame
    void Update()
    {
        //m_rigidbody.AddRelativeTorque(0, 0, Input.GetAxis("Horizontal") * Time.deltaTime * 100, ForceMode.Impulse);
        //m_rigidbody.AddForce(m_cameraTransform.right * Input.GetAxis("Horizontal") * Time.deltaTime * 100, ForceMode.Impulse);
        //m_rigidbody.AddForce(m_cameraTransform.forward * Input.GetAxis("Vertical") * Time.deltaTime * 10, ForceMode.Impulse);
    }


    private void LateUpdate()
    {
        /*
        Vector3 a = m_cameraTransform.rotation * Vector3.forward;
        Vector3 b = m_oldCameraRotation * Vector3.forward;

        m_rigidbody.AddTorque(Vector3.Cross(a, b) * Time.deltaTime * 1000, ForceMode.Impulse);

        m_oldCameraRotation = m_cameraTransform.rotation;
        */

        m_collider.height = (m_cameraTransform.localPosition.y + .2f) * .5f;
        m_collider.center = new Vector3(0, m_collider.height * .5f, 0);
        
    }
}
