﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand : MonoBehaviour
{
    public enum Side { Left, Right }

    public Side m_side;
    Vector3 m_oldPosition;

    public Vector3 WorldspaceVelocity { get; set; }
    private Player m_player;
    
    public LayerMask m_touchLayers;

    public Valve.VR.SteamVR_Behaviour_Skeleton m_skeleton;

    // Start is called before the first frame update
    void Start()
    {
        m_oldPosition = transform.position;
        m_player = GetComponentInParent<Player>();
        m_skeleton = GetComponent<Valve.VR.SteamVR_Behaviour_Skeleton>();
    }

    private RaycastHit m_raycastHit;
    private bool isTouching;

    // Update is called once per frame
    void Update()
    {
    }

    private void LateUpdate()
    {
        WorldspaceVelocity = transform.position - m_oldPosition;
        m_oldPosition = transform.position;

        isTouching = Physics.Raycast(transform.position, (m_side == Side.Right ? -transform.right : transform.right), out m_raycastHit, .05f, m_touchLayers);
    }

    private FixedJoint m_grabJoint;

    private void FixedUpdate()
    {
        float avgCurl = 0;
        for(int i = 0; i < m_skeleton.fingerCurls.Length; i++)
        {
            avgCurl += m_skeleton.fingerCurls[i];
        }
        avgCurl /= m_skeleton.fingerCurls.Length;

        if (isTouching && avgCurl < .2f)
        {
            float dot = Vector3.Dot(-m_raycastHit.normal, WorldspaceVelocity.normalized);
            if (dot >= 0)
            {
                m_player.m_rigidbody.AddForce(-WorldspaceVelocity * Time.fixedDeltaTime * 200, ForceMode.Impulse);
            }
        }
        /*
        if (isTouching)
        {
            if(avgCurl > .75f)
            {
                if(null == m_grabJoint)
                {
                    m_grabJoint = m_player.gameObject.AddComponent<FixedJoint>();
                    m_grabJoint.connectedAnchor = m_player.transform.InverseTransformPoint(transform.position);
                }
            }
            else
            {
                if(null != m_grabJoint)
                {
                    Destroy(m_grabJoint);
                }
            }
        }

        if(null != m_grabJoint)
        {
            transform.position = m_player.transform.TransformPoint(m_grabJoint.connectedAnchor);
        }
        */

        if (avgCurl < .1f)
        {
            if (WorldspaceVelocity.magnitude < 1)
            {
                float dot = Vector3.Dot(WorldspaceVelocity, -m_player.m_cameraTransform.forward);
                if (dot > 0)
                {
                    m_player.m_rigidbody.AddForce(-WorldspaceVelocity * Time.deltaTime * 5000 * dot, ForceMode.Impulse);
                }
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = (m_side == Side.Left ? Color.red : Color.blue);
        Gizmos.DrawSphere(transform.position, .05f);

        Gizmos.DrawLine(transform.position, transform.position + WorldspaceVelocity);
    }
}
