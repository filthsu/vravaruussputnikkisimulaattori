﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poolable : MonoBehaviour
{
    public Pooling.Pool m_owner;
    private Transform m_bindTransform;
    Vector3 m_bindPosition;
    Quaternion m_bindRotation;

    private void LateUpdate()
    {
        if (m_bindTransform)
        {
            transform.position = m_bindTransform.TransformPoint(m_bindPosition);
            transform.rotation = m_bindTransform.rotation * m_bindRotation;
        }
    }

    public void BindToTransform(Transform t, Vector3 localposition, Quaternion localRotation)
    {
        this.m_bindTransform = t;
        this.m_bindPosition = localposition;
        this.m_bindRotation = localRotation;
    }

    private void OnDisable()
    {
        m_bindTransform = null;
        m_owner.Push(this);
    }
}
