﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pooling : MonoBehaviour
{
    [System.Serializable]
    public class PoolInfo
    {
        public string m_name;
        public int m_ID;
        public int m_size = 32;
        public GameObject m_prefab;
    }

    public class Pool
    {
        public PoolInfo m_info;
        public Transform m_transform;
        public Queue<Poolable> objects;

        public Poolable Pop()
        {
            if (objects.Count < 0) Extend();
            Poolable p = objects.Dequeue();
            p.gameObject.SetActive(true);
            return p;
        }

        public void Push(Poolable p)
        {
            objects.Enqueue(p);
        }

        public void Extend(int size = 1)
        {
            for(int i = 0; i < size; i++)
            {
                GameObject go = Instantiate(m_info.m_prefab, m_transform);
                go.SetActive(false);
                Poolable poolable = go.AddComponent<Poolable>();
                poolable.m_owner = this;
                objects.Enqueue(poolable);
            }
        }
    }

    [RuntimeInitializeOnLoadMethod]
    public static void Initialize()
    {
        //Instance = new GameObject("Pooling").AddComponent<Pooling>();
    }

    private static Pooling Instance { get; set; }

    public List<PoolInfo> m_pools;
    Pool[] m_poolArray;

    private void Awake()
    {
        if (Instance)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);

        int idPoolSize = 0;
        for(int i = 0; i < m_pools.Count; i++)
        {
            idPoolSize = Mathf.Max(idPoolSize, m_pools[i].m_ID);
        }
        idPoolSize++;
        m_poolArray = new Pool[idPoolSize];

        for(int i = 0; i < m_pools.Count; i++)
        {
            if(null == m_pools[i].m_prefab) { continue; }
            Transform t = new GameObject(m_pools[i].m_name).transform;
            t.parent = transform;
            m_poolArray[m_pools[i].m_ID] = new Pool()
            {
                m_info = m_pools[i],
                m_transform = t,
                objects = new Queue<Poolable>()
            };
            m_poolArray[m_pools[i].m_ID].Extend(m_pools[i].m_size);
        }
    }

    public Poolable Pop(int id)
    {
        return m_poolArray[id].Pop();
    }

    public Poolable Pop(string name)
    {
        for(int i = 0; i < m_pools.Count; i++)
        {
            if(m_pools[i].m_name == name)
            {
                return Pop(m_pools[i].m_ID);
            }
        }
        return null;
    }
}
