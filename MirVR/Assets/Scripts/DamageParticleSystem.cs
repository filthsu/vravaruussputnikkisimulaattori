﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageParticleSystem : MonoBehaviour
{
    ParticleSystem m_particleSystem;
    [Range(0f, 1f)]
    public float m_healthThreshold = .5f;
    // Start is called before the first frame update
    void Start()
    {
        m_particleSystem = GetComponentInChildren<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if(m_particleSystem && Game.Current)
        {
            if(Game.Current.m_shipHealth < m_healthThreshold)
            {
                if (!m_particleSystem.isPlaying)
                {
                    m_particleSystem.Play();
                }
            }
            else
            {
                if (m_particleSystem.isPlaying)
                {
                    m_particleSystem.Stop();
                }
            }
        }
    }
}
