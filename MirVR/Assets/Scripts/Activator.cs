﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Activator : MonoBehaviour
{
    public static List<Activator> list = new List<Activator>();

    private void OnEnable()
    {
        list.Add(this);
    }

    private void OnDestroy()
    {
        list.Remove(this);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawSphere(transform.position, .02f);
    }
}
