﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushButton : Issue
{
    public float m_activateRadius = .5f;
    
    
    private void Update()
    {
        if(Active)
        {
            for (int i = 0; i < Activator.list.Count; i++)
            {
                if (Vector3.Distance(transform.position, Activator.list[i].transform.position) < m_activateRadius)
                {
                    this.Resolve();
                }
            }
        }
    }
    

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.position, m_activateRadius);
    }
}
