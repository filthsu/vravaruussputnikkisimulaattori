﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotoSwitch : PushButton
{
    public Vector3[] m_rotations;
    public Transform m_rotoSwitch;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        m_rotoSwitch.localRotation = Quaternion.Euler(Vector3.Lerp(m_rotoSwitch.localRotation.eulerAngles, (Active ? m_rotations[0] : m_rotations[1]), Time.deltaTime * 10));
    }
}
